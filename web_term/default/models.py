# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Main(models.Model):
    title = models.CharField(max_length=150)
    description = models.TextField()

    def __str__(self):
        return self.title
